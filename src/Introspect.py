from Server import Server
from Web import Web
from CMS import CMS

from sys import argv

def main():
	host = argv[1]
	cms = CMS()			#Model
	w = Web(cms)		#View
	s = Server(w, host)	#Controller 

if __name__ == '__main__':
    main()
