from wsgiref.simple_server import make_server
from sys import exc_info
from traceback import format_tb
from urlparse import parse_qs
from cgi import FieldStorage, escape
from os.path import join
from os import mkdir, walk, getcwd
from threading import Thread
from cgitb import enable; enable()
from time import sleep
from subprocess import Popen, PIPE

from SimpleHTTPServer import SimpleHTTPRequestHandler
from SocketServer import TCPServer


class ExceptionMiddleware(object):
	def __init__(self, app, server):
		self.app = app
		self.server = server

	def __call__(self, environ, start_response):
		if (environ['REQUEST_METHOD'] == 'POST'):
			form = FieldStorage(fp=environ['wsgi.input'], environ=environ)
			n = form['uploadfile'].filename
			f = form['uploadfile'].file
			with open(n, 'wb') as outfile:
				outfile.write(f.read())
			
			status = '200 OK'
			headers = [('Content-type', 'text/html')]
			start_response(status, headers)
			yield """<HEAD>
						<META http-equiv="refresh" content="0; URL='/'" />
					</HEAD>"""
		else:
			appiter = None
			try:
				appiter = self.app(self, environ, start_response, self.server)
				for item in appiter:
					yield item
			except:
				e_type, e_value, tb = exc_info()
				traceback = ["""<P>
									Traceback (most recent call last):
								</P><PRE>"""]
				traceback += format_tb(tb)
				traceback.append("""
									</PRE><P>%s: %s</P>
								""" % (e_type.__name__, e_value))
				try:
					start_response('500 INTERNAL SERVER ERROR', [
									('Content-Type', 'text/plain')])
				except:
					pass
				yield '\n'.join(traceback)
			if hasattr(appiter, 'close'):
				appiter.close()
	
	def server_app(self, environ, start_response, server):
		status = '200 OK'
		headers = [('Content-type', 'text/html')]
		start_response(status, headers)

		server.view.head()
		path = environ['PATH_INFO']
		query = parse_qs(environ['QUERY_STRING'])
		files = None
		cmd = None
		if ('files' in query):
			files = query['files']
		if ('graphfile' in query):
			graphfile = query['graphfile'][0] + '.jpg'
			g = server.view.model.generate_graph_recursive(getcwd(), files, graphfile)
		elif ('bookfile' in query):
			bookfile = query['bookfile'][0] + '.pdf'
			server.view.model.generate_book(bookfile, files)
		elif ('tarfile' in query):
			tf = query['tarfile'][0]
			server.view.model.generate_tarfile(tf, files)			
		elif ('git' in query):
			git = query['git'][0]
			repo = Popen("git clone %s" % git, 
								shell=True, 
								stdout=PIPE).communicate()[0]
			
		elif ('mkdirfn' in query):
			d = query['mkdirfn'][0]
			mkdir(d)	
		elif ('touchfile' in query):
			touchfile = query['touchfile'][0]
			with open(touchfile, 'a'):
				pass
		elif ('report' in query):
			cmd = query['report'][0]
			name = query['name'][0] + ".report"
			server.view.model.save_code(cmd,name)
		if ('cmd' in query):
			cmd = query['cmd'][0]
			if (cmd == "arch"):
				server.view.model.generate_archetype(query, 
														server.view.arch_dir,
														server.view.home_dir)
				cmd = "archetype"
			elif (cmd == "inst"):
				server.view.model.generate_instance(query, 
													getcwd(),
													server.view.arch_dir)
				cmd = "instance"
			elif (cmd == "edit_instance"):
				server.view.model.generate_instance(query, 
													getcwd(),
													server.view.arch_dir,
													overwrite=True)
				cmd = "edit"
			elif (cmd == "orm_instance"):
				path = join(getcwd(), query['name'][0] + '.csv')
				archetype = query['archetype'][0]
				server.view.model.generate_ormcsv(getcwd(), path, archetype)
				cmd = "orm"
			elif (cmd == "query_instance"):
				n = query['name1'][0] + '.query'
				qz = query['query_text']
				qtypez = query['query_type']
				server.view.model.generate_query(n, files, qz, qtypez)
				cmd = "query"
			func = getattr(server.view, cmd)
			func(files)
		else: 
			server.view.fp(files)
		return server.view.tail()


class Server:
	def __init__(self, view, host):
		self.view = view
		self.host = host
#		self.host = "0.0.0.0"
		self.port1 = 9000
		self.port2 = 8999
		view.port2 = self.port2
		view.port1 = self.port1
		view.host = host
		print host, self.port1,self.port2

		self.httpd_handler = SimpleHTTPRequestHandler
		self.httpd2 = TCPServer((self.host, self.port2), self.httpd_handler)
		print "Controller serving at port", self.port2
		t2 = Thread(target=self.httpd2.serve_forever)
		t2.daemon = True
		t2.start()

		self.middleware = ExceptionMiddleware(
												ExceptionMiddleware.server_app, 
												self
											)
		self.httpd1 = make_server(self.host, self.port1, self.middleware)
		print "Controller Serving on port "+unicode(self.port1)
		t1 = Thread(target=self.httpd1.serve_forever)
		t1.daemon = True
		t1.start()
		while True:
			sleep(1)
