from pickle import dump,load
from os.path import exists, join, isdir, isfile, abspath
from glob import glob
from os import walk
from subprocess import Popen, PIPE
import tarfile
from cgi import escape
from subprocess import check_output
from itertools import izip

from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Image, Paragraph, Preformatted, SimpleDocTemplate

from pygraphviz import AGraph

import Reports

class CMS:
	def __init__(self):
		print "Model\n"

	def generate_instance(self, query, cwd, archdir, overwrite=None):
		self.casting	= True
		self.mold		= False
		self.archdir	= archdir
		self.name 		= query['name'][0]
		self.archetype 	= query['archetype'][0]
		if ('instance' in query):
			self.filename = query['instance'][0]
		else:
			self.dir		= cwd + "/"
			self.filename 	= self.dir + self.name + '.' + self.archetype
		self.moldname 	= self.archdir + self.archetype + ".mold" 
		mold = self.load_archetype(self.moldname)
		self.homedir 	= mold.homedir
		self.data 		= mold.data
		for x in self.data.viewkeys():
			if (str(x) in query):
				self.data[x]['value'] = query[str(x)][0]	
		self._pickle_self(self.filename, overwrite)

	def generate_archetype(self, query, archdir, homedir):
		self.mold		= True
		self.casting	= False
		self.homedir 	= homedir
		self.archdir 	= archdir
		self.name 		= query['name1'][0]
		self.moldname 	= archdir + self.name + '.mold'
		self.archetype  = 'mold'
		self.data = dict.fromkeys(range(1, len(query['name']) + 1))
		for z in self.data.viewkeys():
			self.data[z] = {}
			self.data[z]['name'] = query['name'][z-1]
			self.data[z]['type'] = query['arch_type'][z-1]
		self._pickle_self(self.moldname)

	def generate_view_html(self):
		for z in self.data.viewkeys():
			y = self.data[z]['type']
			if ('textarea' in y):
				self.data[z]['view_html'] = """<TEXTAREA name="%s" readonly>""" % z
				if 'value' in self.data[z]:
					self.data[z]['view_html'] += self.data[z]['value']
				self.data[z]['view_html'] += """</TEXTAREA>""" 
			elif ('text' in y):
				self.data[z]['view_html'] = """<INPUT name="%s" type="text" readonly """ % z
				if 'value' in self.data[z]:
					self.data[z]['view_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['view_html'] += """ />"""
			elif ('number' in y):
				self.data[z]['view_html'] =  """<INPUT name="%s" type="number" readonly """ % z
				if 'value' in self.data[z]:
					self.data[z]['view_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['view_html'] += """/>"""
			elif ('email' in y):
				self.data[z]['view_html'] =  """<INPUT name="%s" type="email" readonly """ % z
				if 'value' in self.data[z]:
					self.data[z]['view_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['view_html'] += """/>"""
			elif ('date' in y):
				self.data[z]['view_html'] =  """<INPUT name="%s" type="date" readonly """ % z
				if 'value' in self.data[z]:
					self.data[z]['view_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['view_html'] += """/>"""
			elif ('time' in y):
				self.data[z]['view_html'] =  """<INPUT name="%s" type="time" readonly """ % z
				if 'value' in self.data[z]:
					self.data[z]['view_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['view_html'] += """/>"""
			elif ('telephone' in y):
				self.data[z]['view_html'] =  """<INPUT name="%s" type="telephone" readonly """ % z
				if 'value' in self.data[z]:
					self.data[z]['view_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['view_html'] += """ />"""
			elif ('url' in y):
				self.data[z]['view_html'] =  """<INPUT name="%s" type="url" readonly """ % z
				if 'value' in self.data[z]:
					self.data[z]['view_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['view_html'] += """/>"""
			elif ('ref' in y):
				self.data[z]['view_html'] =  """<SELECT name="%s" >
													<OPTION value=""></OPTION>""" % z
				for f in self._walkFiles(self.homedir):
					self.data[z]['view_html']+= """<OPTION value="%s" disabled """ % f
					if 'value' in self.data[z]:
						if f == self.data[z]['value']:
							self.data[z]['view_html'] += """selected"""
					self.data[z]['view_html']+= """>%s</OPTION>""" % f
				for d in self._walkDirs(self.homedir):
					self.data[z]['view_html']+= """<OPTION value="%s" disabled """ % d
					if 'value' in self.data[z]:
						if d == self.data[z]['value']:
							self.data[z]['view_html'] += """selected"""
					self.data[z]['view_html']+= """>%s</OPTION>""" % d
				self.data[z]['view_html'] += """</SELECT>""" 

	def generate_edit_html(self):
		for z in self.data.viewkeys():
			y = self.data[z]['type']
			if ('textarea' in y):
				self.data[z]['edit_html'] = """<TEXTAREA name="%s">""" % z
				if 'value' in self.data[z]:
					self.data[z]['edit_html'] += self.data[z]['value']
				self.data[z]['edit_html'] += """</TEXTAREA>""" 
			elif ('text' in y):
				self.data[z]['edit_html'] = """<INPUT name="%s" type="text" """ % z
				if 'value' in self.data[z]:
					self.data[z]['edit_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['edit_html'] += """ />"""
			elif ('number' in y):
				self.data[z]['edit_html'] =  """<INPUT name="%s" type="number" """ % z
				if 'value' in self.data[z]:
					self.data[z]['edit_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['edit_html'] += """/>"""
			elif ('email' in y):
				self.data[z]['edit_html'] =  """<INPUT name="%s" type="email" """ % z
				if 'value' in self.data[z]:
					self.data[z]['edit_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['edit_html'] += """/>"""
			elif ('date' in y):
				self.data[z]['edit_html'] =  """<INPUT name="%s" type="date" """ % z
				if 'value' in self.data[z]:
					self.data[z]['edit_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['edit_html'] += """/>"""
			elif ('time' in y):
				self.data[z]['edit_html'] =  """<INPUT name="%s" type="time" """ % z
				if 'value' in self.data[z]:
					self.data[z]['edit_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['edit_html'] += """/>"""
			elif ('telephone' in y):
				self.data[z]['edit_html'] =  """<INPUT name="%s" type="telephone" """ % z
				if 'value' in self.data[z]:
					self.data[z]['edit_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['edit_html'] += """ />"""
			elif ('url' in y):
				self.data[z]['edit_html'] =  """<INPUT name="%s" type="url" """ % z
				if 'value' in self.data[z]:
					self.data[z]['edit_html'] += """value="%s" """ % self.data[z]['value']
				self.data[z]['edit_html'] += """/>"""
			elif ('ref' in y):
				self.data[z]['edit_html'] =  """<SELECT name="%s" >
													<OPTION value=""></OPTION>""" % z
				for f in self._walkFiles(self.homedir):
					self.data[z]['edit_html']+= """<OPTION value="%s" """ % f
					if 'value' in self.data[z]:
						if f == self.data[z]['value']:
							self.data[z]['edit_html'] += """selected"""
					self.data[z]['edit_html']+= """>%s</OPTION>""" % f
				for d in self._walkDirs(self.homedir):
					self.data[z]['edit_html']+= """<OPTION value="%s" """ % d
					if 'value' in self.data[z]:
						if d == self.data[z]['value']:
							self.data[z]['edit_html'] += """selected"""
					self.data[z]['edit_html']+= """>%s</OPTION>""" % d
				self.data[z]['edit_html'] += """</SELECT>""" 

	def _pickle_self(self, filename, overwrite=None):
		if ((not exists(filename)) or overwrite):
			with open(filename, 'wb+') as f:
				dump(self, f)
		else:
			raise NameError('File %s exists' % filename)
				
	def get_castings(self, cwd, archetype=''):
		z = []
		for a in self._walkFiles(cwd):
			y = self.isCMS(a) 
			if y and y.casting==True and y.mold==False:
				if archetype:
					if y.archetype == archetype:
						z.append(a)
				else:
					z.append(a)
		return z

	def get_molds(self, archdir):
		return glob(archdir + "*.mold")

	def load_archetype(self, path):
		with open(path, 'rb') as f:
			try: 
				x = load(f)
				return x
			except:
				return None

	def isCMS(self, path):
		y = self.load_archetype(path)
		if isinstance(y, CMS):
			return y
		else: return None

	def _walkFiles(self, dir):
		for root, dz, fz in walk(dir, followlinks=True):
			for f in fz:
				yield join(root, f)
	
	def _walkDirs(self, dir):
		for root, dz, fz in walk(dir, followlinks=True):
			for d in dz:
				yield join(root, d + '/')
	
	def generate_book(self, bookfile, files):
			self.styles = getSampleStyleSheet()
			self.stz = self.styles['Normal']
			self.sty = self.styles['Heading1']
			self.stx = self.styles['Heading2']
			self.stw = self.styles['Heading3']
			text = []
			doc = SimpleDocTemplate(bookfile, page='letter')
			text = self._make_book_recursive(text, files)
			doc.build(text)
	
	def _make_book_recursive(self, text, files, dirpath=''):
		for f in files:
			f = join(dirpath, f)
			if (isdir(f)):
				for root, dz, fz in walk(f, followlinks=True):
					text.append(Paragraph('Directory: ' + root, self.sty))
					if fz:
						text = self._make_book_recursive(text, fz, root)
				continue
			else:
				mime = Popen("/usr/bin/file --mime %s" % '"'+f+'"', 
								shell=True, 
								stdout=PIPE).communicate()[0]
				text.append(Paragraph(f, self.stx))
				text.append(Paragraph(mime, self.stw))
				g = self.isCMS(f)
				if g:
					text.append(Paragraph(g.name, self.stw))
					for h in g.data.viewkeys():
						text.append(Paragraph(g.data[h]['name'], self.stw))
						if 'value' in g.data[h]:
							text.append(Preformatted(g.data[h]['value'], self.stz))
				elif " image/" in mime:			
					text.append(Image(f, 
									width=500,
									height=500,
									kind='proportional'))
				elif "text" in mime:
					with open(f, 'rb') as l:
						text.append(Preformatted(l.read(), self.stz))
				else:
					pass
		return text

	def generate_tarfile(self, tf, files):
		with tarfile.open(tf+".tar", "w") as tar:
			if files[0] == "..":
				tar.add(files[0])
			else:
				for f in files:
					tar.add(f)
	
	def generate_ormcsv(self, cwd, path, archetype):
		castings = self.get_castings(cwd, archetype=archetype)
		with open(path, "w+") as f:
			line = '';
			q = self.isCMS(castings[0])
			line += "NAME , "
			for y in q.data.viewkeys():
				line += (q.data[y]['name'] + ' , ')
			line += ' \n'
			f.write(line)
			for c in castings:
				line = '';
				casting = self.isCMS(c)
				line += casting.name + ' , '
				for x in casting.data.viewkeys():
					if ('value' in casting.data[x]):
						line += (casting.data[x]['value'].encode('string_escape') + ' , ')
					else:
						line += ' , '
				line += ' \n'
				f.write(line)

	def checkRefs(self, cwd, cms):
		l = []
		self = cms
		for z in self.data.viewkeys():
			if 'value' in self.data[z]:
				v = self.data[z]['value']
				if self.data[z]['type'] == 'ref':
					l.append(join(cwd,v))
		if l.count > 0: return l
		else: return None
		
	def generate_graph_recursive(self, cwd, files, name, graph=None, dir=None):
		if graph:
			g = graph
		else:
			g = AGraph(directed=True, rankdir='LR') 
		l = []
		for f in files:
			y=join(cwd,f)
			if not graph:
				g.add_node(y)	
			else:
				l.append(f)
				g.add_node(f)
			if isfile(str(f)):
				z = self.isCMS(f)
				if z:
					p = self.checkRefs(cwd, z)
					if p:
						for i in p:
							g.add_edge(i.rstrip('\/') , y)
			elif isdir(str(f)):
				for root, dz, fz in walk(y, followlinks=True):
					fy,dy = [], []
					for f in fz:
						fy.append(join(root,fz[fz.index(f)]))
					for d in dz:
						dy.append(join(root,dz[dz.index(d)]))
					g = self.generate_graph_recursive(cwd, fy, name, graph=g, dir=root)
					g = self.generate_graph_recursive(cwd, dy, name, graph=g, dir=root)
		if l and dir:
			g.add_subgraph(nbunch=l, name="cluster_"+dir, label=dir)
		if graph:		
			return g
		else:
			g.draw(name, prog="dot")	

	def generate_query(self, name, files, querys, query_types):
	#TODO queries only search by name for now
		r = []
		for q,t in izip(querys,query_types):
			if t == "name":
				 for l in check_output("find . -iname '*%s*'" % q, shell=True).splitlines():
					r.append(l)
			if t == "searchable":
				pass
			if t == "type":
				pass
			if t == "location":
				pass
		with open(name, "w+") as f:
			for l in r:
				print l
				f.write(l+"\n")

	def load_code(self, file):
		with open(file, "r+") as f:
			cmd = f.read()
		return cmd

	def save_code(self, cmd, name):
		with open(name, "w+") as f:
			f.write(cmd)

	def exec_code(self, cmd):
		exec(cmd)

