from os import getcwd, listdir, chdir, environ, makedirs, rename, walk, unlink
from os.path import isdir, isfile, exists, join, basename, splitext
from shutil import move, rmtree

class Web:
	def __init__(self, model):
		self.model = model
		print "View\n"
		self.home_dir = environ['HOME'] + '/'
		self.conf_dir = self.home_dir + ".introspect/"
		self.cut_dir = self.conf_dir + 'cut/'
		self.arch_dir = self.conf_dir + 'Archetypes/'
		self.setDirectory(self.home_dir)
		if (not exists(self.conf_dir)):
			makedirs(self.arch_dir)
			makedirs(self.cut_dir)

	def setDirectory(self, dir):
		chdir(dir)
		self.directory = getcwd()

	def getDirectory(self):
		d = listdir(self.directory)
		d.insert(0,'..')
		return d
	
	def head(self):
		self.string ="""<HTML>
							<HEAD><TITLE>Introspect Manager</TITLE>
								<META charset="UTF-8">
								<H1><a href="http://%s:%d">INTROSPECT</a></H1>
								<P>%s</P>
								<HR/>
							</HEAD>
							<BODY style="background-color:dimgray;">
					""" % (self.host, self.port1, getcwd())
	def tail(self):
		self.string += """	</BODY>
						</HTML>"""
		return self.string

	def fp(self, files):
		self.string += """<FORM id="form">
							<INPUT type="submit">
						</FORM>
						<SELECT name="cmd" id="cmd" form="form" onchange="ins_opt();">
							<OPTION value=""></OPTION>
							<OPTION value="archetype">Archetype...</OPTION>
							<OPTION value="book">Book</OPTION>
							<OPTION value="cut">Cut</OPTION>
							<OPTION value="delete">Delete Clipboard</OPTION>
							<OPTION value="edit">Edit Instance...</OPTION>
							<OPTION value="file">File Download...</OPTION>
							<OPTION value="graph">Graph</OPTION>
							<OPTION value="home">Home</OPTION>
							<OPTION value="instance">Instance...</OPTION>
							<OPTION value=""></OPTION>
							<OPTION value="klone">Klone</OPTION>
							<OPTION value=""></OPTION>
							<OPTION value="mkdir">Make Directory</OPTION>
							<OPTION value=""></OPTION>
							<OPTION value="orm">ORM...</OPTION>
							<OPTION value="paste">Paste</OPTION>
							<OPTION value="query">Query...</OPTION>
							<OPTION value="report">Report</OPTION>
							<OPTION value="script">Run Report</OPTION>
							<OPTION value="touch">Touch</OPTION>
							<OPTION value="upload">Upload...</OPTION>
							<OPTION value="view">View</OPTION>
							<OPTION value=""></OPTION>
							<OPTION value=""></OPTION>
							<OPTION value=""></OPTION>
							<OPTION value="zip">Zip</OPTION>
						</SELECT>
						</BR>
						<FIELDSET id="optional" form="form"></FIELDSET>
						</BR>
							<SELECT name="files" size="36" form="form" multiple>
					"""
		for f in self.getDirectory():
			self.string += """	<OPTION value="%s">
									%s
								</OPTION>""" % (f,f)
		self.string += """</SELECT>"""
		self.string += """<SCRIPT>
							function ins_opt(){
								var s = document.getElementById("cmd").value;
								var x = document.getElementById("optional");
								switch (s){
									case (""): 
										string = "";
										break;
									case ("book"):
										string = `<LABEL>Name:</LABEL>
													<INPUT type="text"
															name="bookfile"
															form="form"
															required />
													<LABEL>.pdf</LABEL>`;
										break;
									case ("graph"):
										string = `<LABEL>Name:</LABEL>
													<INPUT type="text" 
															name="graphfile" 
															form="form"
															required />
													<LABEL>.jpg</LABEL>`;
										break;		
									case ("klone"):
										string = `<LABEL>GIT REPO:</LABEL>
													<INPUT type="text"
															name="git"
															form="form"
															required />
												`;
										break;
									case ("mkdir"):
										string = `<LABEL>Name:</LABEL>
													<INPUT type="text" 
														name="mkdirfn"
														form="form"
														required />`;
										break;
									case ("touch"):
										string = `<LABEL>Name:</LABEL>
													<INPUT type="text"
														name="touchfile" 
														form="form" 
														required />`;
										break;
									case ("report"):
										string = `	<LABEL>Name: </LABEL> 
													<INPUT type="text"
															name="name"
															form = "form"
															required />
													<LABEL>.report</LABEL>
													</br>
													<LABEL>Code:</LABEL>
													<TEXTAREA
															name = "report"
															form = "form"
															required></TEXTAREA>
												`;
										break;
									case ("zip"):
										string =
											`<LABEL>Name:</LABEL>
												<INPUT type="text" 
													name="tarfile" 
													form="form" 
													required />
											<LABEL>.tar</LABEL>`;
										break;
									default:
										string = '';
										break;
								}
								x.innerHTML = string;
							}
						</SCRIPT>"""

	def archetype(self, files):
		self.string += """
						<FORM id="form">
							<INPUT type="hidden" name="cmd" value="arch">
							<INPUT type="submit">
							<LABEL>Name:</LABEL>
							<INPUT type="text" name="name1" required />
							<LABEL>.mold</LABEL>
							<BUTTON type="button" 
									onclick="insert_li();">Add List Item</BUTTON>
							<DIV id="architype">
								<FIELDSET name="arch_item" id="arch_item">
									<OL id="arch_list">
									</OL>
								</FIELDSET>
							</DIV>
						</FORM>
						<SCRIPT>
							function insert_li(){
								var l = document.getElementById("arch_list");
								var li = document.createElement("LI");
								li.innerHTML = `
											<LABEL>Name:</LABEL>
											<INPUT type="text" name="name" required />
											<SELECT name="arch_type" id="arch_type" />
												<OPTION value="text">Text</OPTION>
												<OPTION value="textarea">Textarea</OPTION>
												<OPTION value="number">Number</OPTION>
												<OPTION value="email">Email</OPTION>
												<OPTION value="date">Date</OPTION>
												<OPTION value="time">Time</OPTION>
												<OPTION value="telephone">Telephone</OPTION>
												<OPTION value="url">URL</OPTION>
												<OPTION value="ref">Reference</OPTION>
											</SELECT>
											<BUTTON type="button" 
													onclick="delete_li(this);">x</BUTTON>
											`;

								l.appendChild(li);
							}
							function delete_li(button){
								button.parentElement.remove();
							}
						</SCRIPT>
						"""

	def book(self, files):
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""
	
	def cut(self, files):
		for f in files:
			d = self.cut_dir + f
			rename(f, d)
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""

	def delete(self, files):
		for root, dz, fz in walk(self.cut_dir):
			for f in fz:
				unlink(join(root, f))
			for d in dz:
				rmtree(join(root, d))
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""

	def edit(self, files):
		b = {}
		self.string += """<FORM id="form">	
								<INPUT type="hidden" name="cmd" value="edit_instance">
								<SELECT name="instance" onchange="show_inst(this.value)">
									<OPTION value=""></OPTION>
						"""
		for a in self.model.get_castings(getcwd()):
			b[a] = self.model.load_archetype(a)
			b[a].generate_edit_html()
			self.string += """ 	`	<OPTION value"=%s">
										%s
									</OPTION>""" % (a,a)
		self.string += """		</SELECT>
								<DIV id="instance">
									<FIELDSET name="inst_item" id="inst_item">
										<!---->
									</FIELDSET>
								</DIV>
								<INPUT type="submit" />
							</FORM>
					
							<SCRIPT>
								var inst = new Object();"""
		for c in b.viewkeys():
			self.string += """	 inst['%s'] = `<INPUT type="hidden" name="archetype" value="%s" />
												<LABEL>Name:</LABEL>
												<INPUT type="text" name="name" value="%s" readonly />
												<LABEL>.%s</LABEL>
												<OL>
														""" % (c, 
																b[c].archetype, 
																b[c].name, 
																b[c].archetype)
			for d in b[c].data.viewkeys():
				self.string += """	
													<LABEL>%s</LABEL>
													<LI>%s</LI>
														 """ % ( b[c].data[d]['name'], 
																b[c].data[d]['edit_html'])
			self.string += """					</OL>`; 
							"""
		self.string +=	"""
								function show_inst(val){
									var f = document.getElementById("inst_item");
									f.innerHTML = inst[val];
								}
							</SCRIPT>
						"""
	

	def file(self, files):
		self.string += """<IFRAME src="http://%s:%d"></IFRAME>""" % (self.host, self.port2)

	def graph(self, files):
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""

	def home(self, files):
		self.setDirectory(self.home_dir)
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""

	def instance(self, files):
		b = {}
		self.string += """<FORM id="form">	
								<INPUT type="hidden" name="cmd" value="inst">
								<SELECT name="archetype" onchange="show_inst(this.value)">
									<OPTION value=""></OPTION>
						"""
		for a in self.model.get_molds(self.arch_dir):
			z = splitext(basename(a))[0]
			b[z] = self.model.load_archetype(a)
			b[z].generate_edit_html()
			self.string += """ 	`	<OPTION value"=%s">
										%s
									</OPTION>""" % (z,z)
		self.string += """		</SELECT>
								<DIV id="instance">
									<FIELDSET name="inst_item" id="inst_item">
										<!---->
									</FIELDSET>
								</DIV>
								<INPUT type="submit" />
							</FORM>
					
							<SCRIPT>
								var inst = new Object();"""
		for c in b.viewkeys():
			self.string += """	 inst['%s'] = `<LABEL>Name:</LABEL>
												<INPUT type="text" name="name" required />
												<LABEL>.%s</LABEL>
												<OL>
														""" % (c, c)
			for d in b[c].data.viewkeys():
				self.string += """	
													<LABEL>%s</LABEL>
													<LI>%s</LI>
														 """ % ( b[c].data[d]['name'], 
																b[c].data[d]['edit_html'])
			self.string += """					</OL>`; 
							"""
		self.string +=	"""
								function show_inst(val){
									var f = document.getElementById("inst_item");
									f.innerHTML = inst[val]
								}
							</SCRIPT>
						"""

	def klone(self, files):
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""
		
	def mkdir(self, files):
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""
			
	def orm(self, files):
		b = {}
		self.string += """<FORM id="form">	
								<INPUT type="hidden" name="cmd" value="orm_instance">
								<SELECT name="archetype" required>
									<OPTION value=""></OPTION>
						"""
		for a in self.model.get_molds(self.arch_dir):
			z = splitext(basename(a))[0]
			b[z] = self.model.load_archetype(a)
			b[z].generate_edit_html()
			self.string += """ 	`	<OPTION value"=%s">
										%s
									</OPTION>""" % (z,z)
		self.string += """		</SELECT>
								<LABEL>Name:</LABEL>
								<INPUT type="text" name="name" required />
								<LABEL>.orm</LABEL>							
								<INPUT type="submit" />
							</FORM>
						"""

	def paste(self, files):
		fz = listdir(self.cut_dir)
		for f in fz:
			move(self.cut_dir+f, self.directory)
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""
			
	def query(self, files):
		self.string += """
						<FORM id="form">
							<INPUT type="hidden" name="cmd" value="query_instance">
							<INPUT type="submit">
							<LABEL>Name:</LABEL>
							<INPUT type="text" name="name1" required />
							<LABEL>.query</LABEL>
							<BUTTON type="button" 
									onclick="insert_li();">Add Query</BUTTON>
							<DIV id="query">
								<FIELDSET name="query_item" id="query_item">
									<OL id="query_list">
									</OL>
								</FIELDSET>
							</DIV>
						</FORM>
						<SCRIPT>
							function insert_li(){
								var l = document.getElementById("query_list");
								var li = document.createElement("LI");
								li.innerHTML = `
											<SELECT name="query_type" id="query_type" />
												<OPTION value="name">Name</OPTION>
												<OPTION value="searchable" disabled>Searchable Text</OPTION>
												<OPTION value="type" disabled>Type</OPTION>
												<OPTION value="location" disabled>Location</OPTION>
											</SELECT>
											<INPUT type="text" name="query_text" />
											<BUTTON type="button" 
													onclick="delete_li(this);">x</BUTTON>
											`;

								l.appendChild(li);
							}
							function delete_li(button){
								button.parentElement.remove();
							}
						</SCRIPT>

						"""

	def touch(self, files):
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""

	def report(self, files):
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""

	def script(self, files):
		c = self.model.load_code(files[0])
		self.model.exec_code(c)

	def upload(self, files):
		self.string += """<FORM id="form" 
								method="POST" 
								enctype="multipart/form-data"
								action="/">
								<INPUT type="file" 
										name="uploadfile" 
										accept="media_type"
										required />
								<INPUT type="submit">
							</FORM>
						"""

	def view(self, files):
		d = self.port2
		e = self.host
		for f in files:
			if (isdir(f)):
				self.setDirectory(f)
				self.string += """<META http-equiv="refresh" 
										content="0; URL='/'" />"""
			elif (isfile(f)):
				j = self.model.isCMS(join(getcwd(),f))
				if j:
					j.generate_view_html()

					self.string += """	  <LABEL>Name:</LABEL>
											<INPUT type="text" 
													name="name" 
													value="%s" 
													readonly />
											<OL>""" % j.name
					for d in j.data.viewkeys():
						self.string += """	
												<LABEL>%s</LABEL>
												<LI>%s</LI>
													 """ % ( j.data[d]['name'], 
															j.data[d]['view_html'])
					self.string += """		</OL>"""
				else:
					with open(f) as o:
						self.string += """<OBJECT 
											width="500" height="500"
											data="http://%s:%d/%s">"""%(e,d,f)
						self.string += """%s</OBJECT>"""%f

	def zip(self, files):
		self.string += """<META http-equiv="refresh" content="0; URL='/'" />"""
		
