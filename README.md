# Introspect

## UNIX manager

Introspect is an object oriented web content managagment system / database built on UNIX

## Installation

Recommended to use Docker with the provided Dockerfile.

You need `file` command and python libraries 'pygraphviz' & 'reportlab'
Along with Python 2

`apt-get install git file python2 python-pygraphviz python-reportlab`

`git clone https://bitbucket.org/mieszkowski/Introspect.git`


## Usage
run

 `python2 introspect/src/Introspect.py 127.0.0.1`
 and visit with your browser `http://127.0.0.1:9000`


## Details

Introspect uses UNIX and Python to implement an Object Oriented Database. The data of the objects
is stored in Python pickles called Instances and the methods are what UNIX and Python offer. So queries are
supported using the `find` command, erds ard done with pygraphviz, etc.

This type of object oriented database 
might be called a [Filesystem Relational Database](https://en.wikipedia.org/wiki/Object%E2%80%93relational_database). The concept uses Relational DB theory as a way to restrict yourself over Object Oriented technology.
It uses objects on a filesystem to form the functionality of the database.
The filesystem and relational DB methods are used to restrict what the database can do and force data normalization.

                interface | implementation | level
                -----------------------------------
                class     | object         | 0 (python/program level)
                archetype | instance       | 1 (database/filesystem level)
                
An Instance is a row. An Archetype defines the columns of a table. 

Nested directories in the filesystem implement database partitioning. 
So it pays to wisely consider your directory structure. As a general rule all objects should go in the same directory. 
At least all objects of the same type, same date, etc. A table is contained in a directory, composed of one or more
object types.

There is no need for index keys because the filesystem maintains its own Unique Ids in the form of file inodes. 
On the other hand nothing is stopping you from using primary keys. Foreign keys are the references, which in Introspect are filenames, a reference to a file.

## Instructions

Introspect is a simple web app that uses a dropdown menu to operate on the current working directory.

Use the Archetype command to make a new database schema.
Use Instance to make a new pickle file e.g. instance.archetype or row.table
Use Edit Instance command to edit row data.

The View command will do different things: eg change to the directory if a directory is selected 
or view an image or pdf. Text files or other binary files are special to a web browser apparently 
so you must use the Download File... command to 
first download the text file from the db.
File Download is implemented on port 8999 

Home brings you back to your home directory.

ORM command will make a comma separated values list of all the Instances it finds recursively from where you
are down. You select the type.

Graph, Book and Zip will take the selected files and make a JPG ERD, PDF or tar archive.

Introspect DB can be seen as restrictive or overly simple.
The goal is normalization of data.  
Examples include; no lists or special data in Archetypes, only simple data and references to files. 
References from Instances to other Instances implements relational references.
The lists in your data need to be normalized out to the filesystem with another type. 
Another example of restrictiveness is that you cannot rename files or edit Archetypes with the interface. 
This is to (encorage) referential integrity. 
Another example is that there is no inheritance in the data objects. 
There is no inheritance in a relational database so there is no inheritance in IntrospectDB.

Internally Introspect uses MVC to make a CMS Model, HTML/Javascript View, and Python Controller. Both Archetypes and Instances
in the database are pickled versions of the CMS class in the program which is the data Model.

Reports in the database use Python, and along with using pickles pose a serious security threat.  
However, pickles are used only for data. Do not use data or reports from untrusted sources.
As an example report to count the files in a directory (including the report):

```import os```

```list = os.listdir('.') # dir is your directory path```

```number_files = len(list)```

```print number_files```


The output goes to the console and you can't really access anything. 

## History

Introspect Database was written to replace a plant database. Something you might use Microsoft Access for.
The original used the [Plone](http://Plone.org) CMS and custom graphing Products. 
It is worth noting that Plone runs likely ~1,000,000 lines of code while
Introspect Database is ~1000.