#docker volume create dbz
#docker build -t introspect --build-arg IP=172.17.0.2 .
#docker run  -it -p 9000:9000 -p 8999:8999 -v dbz:/home/tmp --rm introspect:latest 
FROM debian:buster
ARG USER="tmp"
ARG IP="0.0.0.0"
ENV IP=$IP
RUN apt-get update && apt-get install -y python2 python-reportlab file python-pygraphviz git
RUN groupadd -r $USER && useradd --no-log-init -r -m -d /home/$USER -g $USER $USER
USER $USER
RUN mkdir /tmp/introspect/
EXPOSE 9000 8999
RUN ip -o a
COPY . /tmp/introspect
CMD python2 "/tmp/introspect/src/Introspect.py" "${IP}"

